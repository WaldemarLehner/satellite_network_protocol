import os;
import setuptools

setuptools.setup(
    name="satellite-network-protocol",
    version=os.getenv("PIP_VERSION") or "0.0.1",
    author="Julian Dietzel",
    author_email="julian.dietzel@gmail.com",
    description="Satellite Network Protocol",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
