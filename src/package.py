"""
Header:

                     1 1      2 2      3 3      3 4      4 4      5 5      6 6      7
    │0      7│8      5│6      3│4      1│2      9│0      7│8      5│6      3│4      1
    │XXXXXXXX XXXXXXXX│XXXXXXXX XXXXXXXX│CLSIT   │XXXXXXXX│XXXXXXXX XXXXXXXX XXXXXXXX│
                                         ││││││││    │                  └───────────── [Subpackage number if not control]
             │                 │         ││││││││    └──────────────────────────────── [Message ID        if not control]
             │                 │         │││││││└───────────────────────────────────── [Unused]
             │                 │         ││││││└────────────────────────────────────── [Unused]
             │                 │         │││││└─────────────────────────────────────── [Unused]
             │                 │         ││││└──────────────────────────────────────── T: Is type text
             │                 │         │││└───────────────────────────────────────── I: Is type image
             │                 │         ││└────────────────────────────────────────── S: Sender type: 0=GroundControl, 1=Rover
             │                 │         │└─────────────────────────────────────────── L: Is last package of message
             │                 │         └──────────────────────────────────────────── C: Is control message
             │                 └────────────────────────────────────────────────────── [Receiver ID]
             └──────────────────────────────────────────────────────────────────────── [Sender ID]

Client numbers:

    Client numbers are assigned to every client as the first message from the
    server to the client

    The client number is 2 byte long and therefore in the range
    1 - 65534

    Client number 0     is reserved for the server
    Client number 65535 is the broadcast address

    The assignment message has the form:

    │[SenderID: 0]│[ReceiverID: newID]│10000000│00000001|[2 byte long newID]
"""

from satellite_network_protocol.src.FlagsMask import FlagsMask

HEADER_SIZE = 9
MAX_PACKAGE_SIZE = 4096
PAYLOAD_SIZE = MAX_PACKAGE_SIZE - HEADER_SIZE


class Package:
    def __init__(self, byteMessage=None):
        self.senderID = 0
        self.recvID = 0
        self.isControl = False
        self.isLast = True
        self.senderType = 'ground'
        self.payloadType = 'undefined'
        self.payload = b''
        self.isBroadcast = False
        self.number = 0
        self.messageID = 0

        if byteMessage is not None:
            self.fromByteMessage(byteMessage)

    def fromByteMessage(self, byteMessage):
        self.senderID = int.from_bytes(byteMessage[:2], byteorder='big')
        self.recvID = int.from_bytes(byteMessage[2:4], byteorder='big')

        flagsSection = int.from_bytes(byteMessage[4:5], byteorder='big')
        self.isControl = (flagsSection & FlagsMask.CONTROL_FLAG) != 0
        self.isLast = (flagsSection & FlagsMask.LAST_FLAG) != 0
        self.senderType = 'rover' if (flagsSection & FlagsMask.SENDER_FLAG) != 0 else 'ground'

        if (flagsSection & FlagsMask.TYPE_IMG_FLAG) != 0:
            self.payloadType = 'img'
        elif (flagsSection & FlagsMask.TYPE_TXT_FLAG) != 0:
            self.payloadType = 'txt'
        else:
            self.payloadType = 'undefined'

        if not self.isControl:
            # Getting [5:6] returns the same as [5] but as bytes instead of int
            self.messageID = int.from_bytes(byteMessage[5:6], byteorder='big')
            self.number = int.from_bytes(byteMessage[6:9], byteorder='big')
            self.payload = byteMessage[9:]
        else:
            self.payload = byteMessage[5:]

    def toByteMessage(self):
        header = self.senderID.to_bytes(length=2, byteorder='big')

        if self.isBroadcast:
            header += int(65535).to_bytes(length=2, byteorder='big')
        else:
            header += self.recvID.to_bytes(length=2, byteorder='big')

        flags = 0
        if self.isControl:
            flags |= FlagsMask.CONTROL_FLAG

        if self.isLast:
            flags |= FlagsMask.LAST_FLAG

        if self.senderType == 'rover':
            flags |= FlagsMask.SENDER_FLAG

        if self.payloadType == 'img':
            flags |= FlagsMask.TYPE_IMG_FLAG
        elif self.payloadType == 'txt':
            flags |= FlagsMask.TYPE_TXT_FLAG

        header += flags.to_bytes(length=1, byteorder='big')
        messageID = self.messageID.to_bytes(length=1, byteorder='big')
        number = self.number.to_bytes(length=3, byteorder='big')

        if self.isControl:
            content = header + self.payload
            # Control packages can have at max 128 byte
            content = content[:128]
        else:
            content = header + messageID + number + self.payload

        if len(content) > MAX_PACKAGE_SIZE:
            raise Exception('Payload too large!')

        return content

    def configAsCtrl(self, ctrlMessageType, arg=b'', senderID=0, recvID=0):
        self.recvID = recvID
        self.isControl = True
        self.isLast = False
        self.payloadType = 'undefined'

        self.payload = ctrlMessageType.to_bytes(1, 'big') + arg
