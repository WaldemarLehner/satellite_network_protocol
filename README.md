# Satellite Network Protocol

## About

This is a package with utilities to send and receive packages according to the self defined Satellite Network Protocol (SNP) which is running on top of websockets. This repo is not made to be executed on it's own, but to be included in the server and clients.

## Packages

Messages are sent as packages with a maximum total size of 4096 bytes each. Every standard package has a 9 byte header and as a result a maximum of 4087 byes as payload.
Control messages have a header of 5 bytes (omitting the last 4) and a maximum total size of 128 bytes.
The header of each standard package has the following shape (control packages have the same shape but omit the last 4 byte):

```
 Header:
                                                 │---- Only for standard package ----│
                     1 1      2 2      3 3      3 4      4 4      5 5      6 6      7
    │0      7│8      5│6      3│4      1│2      9│0      7│8      5│6      3│4      1
    │XXXXXXXX XXXXXXXX│XXXXXXXX XXXXXXXX│CLSIT   │XXXXXXXX│XXXXXXXX XXXXXXXX XXXXXXXX│
                                         ││││││││    │                  │ 
             │                 │         ││││││││    │                  └─ [Subpackage number]
             │                 │         ││││││││    └─ [Message ID]
             │                 │         │││││││└────── [Unused]
             │                 │         ││││││└─────── [Unused]
             │                 │         │││││└──────── [Unused]
             │                 │         ││││└───────── T: Is type text
             │                 │         │││└────────── I: Is type image
             │                 │         ││└─────────── S: Sender type: 0=GroundControl, 1=Rover
             │                 │         │└──────────── L: Is last package of message
             │                 │         └───────────── C: Is control message
         [Sender ID]     [Receiver ID]
         
```


### Standard Packages

A standard package sends either text or an image to a (or multiple) receiver(s). The C-Flag needs to be 0. All other flags have to be set according to the message.

Since packages can arrive out of order, a numbering system was added to the end of the header of each standard package.
This end consists of the `Message ID` and the `Subpackage number` and has to be created by the sender:
- `Message ID`: The message ID identifies each message of a sender. It exists to prevent the package of two or more messages getting mixed up when the packages are delivered out of order.
  Each client can decide on his own strategy of picking the message ID, though a Round Robin (always pick the next higher number. If the next higher number is 256, start again from 0) is the best as it has the lowest chance of another client receiving two packages with the same id from one client.
- `Subpackage number`: When a message is larger than the maximum size of 4096 bytes, it is broken up into multiple subpackages. To restore the message even when the packages arrive out of order, the receiver will use the subpackage number to reorder the packages. The first package should start with the number 0. All following packages should simply carry their position in the message (1, 2, 3, ...) as their subpackage number. The receiver knows that he has received all packages of a message when he has received all packages from 0 to the package number of the package where the `last`-flag was set.


### Control Packages

Control packages are special in the sense, that they don't carry data but control messages for the client or server.
Their first byte after the header defines the type of the control message. The other bytes can carry arguments according to the type.
Control packages can have a maximum size of 128 bytes.

The types so far defined are:

* **Connection Refused**: Sent by server for various reasons if the client is not allowed to connect.
    ```
    |[0]|[optional: Reason for refusal]|
    ```
* **New Client ID**: Sent by server to tell a client his ID. Also used to accept a clients connection request, initially telling him his ID.
    ```
    |[1]|[2 byte long ID]|
    ```
* **Connection Request**: Sent by client as his first message with the connection token. The respons will either be a Connection Refused- or a New Client ID message.
    ```
    |[2]|[max. 122 byte token]|
    ```

    Client numbers are assigned to every client as the first message from the
    server to the client


## IDs

Every client has a unique ID, assigned to it by the server after the inital **Connection Request**.
The client number is 2 byte long and can be in the range from 1 to 65534

There are two special, reservered, IDs:
* ID 0     is reserved for the server
* ID 65535 is the broadcast address


## Intial Handshake

There is a very simple handshake between the client and the server at the very beginning of every connection, before the client is allowed to send data:

```
1. [Client: Connection Request] with the token of the channel he wants to connect to
if the clients message was a Connection Request and the token was correct:
    2. [Sever: New Client ID] with the new unique ID of the client
else:
    2. [Server: Connection Refused] with a reason as to why the connection was refused.
```

After this handshake the connection is either disconnected by the server or the client is now allowed to send messages to the channel he connected to.
